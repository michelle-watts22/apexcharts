var ajax = new XMLHttpRequest();
ajax.onload = function (e) {
  var spriteDiv = document.createElement("div");
  spriteDiv.setAttribute("id", "spriteDiv");
  spriteDiv.setAttribute("style", "height: 1px; position: fixed;");
  spriteDiv.innerHTML = ajax.responseText;
  // sprites must be injected at the end for IE11 >:-(
  document.body.appendChild(spriteDiv);
  // svg polyfill for IE11
  svg4everybody();
};
ajax.open("GET", "https://cdn.jsdelivr.net/npm/@nswdoe/app-icons/sprite.svg", true);
ajax.send();
