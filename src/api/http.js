import axios from 'axios';
import qs from 'qs';

/**
 * Create an axios instance and configure THIS instance.
 * If directly import axios and use, settings here won't take effect.
 * Default headers is 'Content-Type': 'application/json'
 */
const requestConfig = {
  // API base URL is configurable and set up in .env.* files
  baseURL: process.env.VUE_APP_API_BASE_URL,
  timeout: 10000,
}
const axiosInstance = axios.create(requestConfig);

/**
 * GET method
 * @param url
 * @param config
 * @returns {Promise}
 */
export function get(url, config = {}) {
  return axiosInstance.get(url, config);
}

/**
 * POST method
 * by default, axios uses JSON format - e.g. 'Content-Type': 'application/json'
 * @param url
 * @param data
 * @param config
 * @returns {Promise}
 * @param isForm determines headers['Content-Type']
 */
export function post(url, data = {}, config = {}, isForm = false) {
  if (isForm) {
    axiosInstance.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    axiosInstance.defaults.transformRequest = data => qs.stringify(data);
  } else { // axios default
    axiosInstance.defaults.headers['Content-Type'] = 'application/json';
    axiosInstance.defaults.transformRequest = undefined;
  }
  return axiosInstance.post(url, JSON.stringify(data), config);
}

/**
 * PUT method
 * by default, axios uses JSON format - e.g. 'Content-Type': 'application/json'
 * @param url
 * @param data
 * @param config
 * @returns {Promise}
 * @param isForm determines headers['Content-Type']
 */
export function put(url, data = {}, config = {}, isForm = false) {
  if (isForm) {
    axiosInstance.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded';
    axiosInstance.defaults.transformRequest = data => qs.stringify(data);
  } else { // axios default
    axiosInstance.defaults.headers['Content-Type'] = 'application/json';
    axiosInstance.defaults.transformRequest = undefined;
  }
  return axiosInstance.put(url, JSON.stringify(data), config);
}


// Can add JWT token here for each request
/*axiosInstance.interceptors.request.use(
  function () {
  },
  function () {
  }
);*/

// Can check HTTP response code here and jump to different page (e.g. Offline Page)
/*axiosInstance.interceptors.request.use(
  function () {
  },
  function () {
  }
);*/

export default axiosInstance;
















