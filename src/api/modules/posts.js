import {get, post} from '../http';

const fetchPosts = (url, config = {}) => get(url, config);
const postPosts = (url, data = {}, config = {}, isForm = false) => post(url, data, config, isForm)

export default {
  fetchPosts,
  postPosts
};
