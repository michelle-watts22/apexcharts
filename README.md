# doe-ui-template

## Introduction
`doe-ui-template` is a template project created using `vue-cli`, `vuetify` and `doe-ui-core`.
It is recommended to download `doe-ui-template` for new ADS projects development because `doe-ui-template` uses `doe-ui-core` (please see `package.json`) and gets everything configured already.

###Pre-configuration includes:
* Settings for Vuetify and MyEssentials icons in `index.html`
* ADS colors for Vuetify themes in `/plugins/vuetify.js`
* `public/favicon.ico` is replaced with the default NSW Government favicon
* A global event bus is created in `main.js`
* `mixins` directory is added under `/src`
* Auto set light/dark theme based on the current Operating System's theme setting ( in `App.vue` )
* `.editorconfig` file is added
* `.gitignore` file is added
* `.eslintrc` file is configured for Vue.js and ADS
* `jsconfig.json` file is add for IDEs intelliSense support for the `@` sign in the `import` statement

## ADS Document
[https://designsystem.education.nsw.gov.au/](https://designsystem.education.nsw.gov.au/)

Please take a look at the ADS Document site where you can find detailed info about ADS components, component design patterns, etc.
A detailed introduction of the doe-ui-template project structure under the Developer's Guide section.

## Project setup
### Install dependencies
```
yarn install
```
### Compiles and hot-reloads for development
```
yarn serve
```
```
yarn serve:test
```
```
yarn serve:pre
```
```
yarn serve:prod
```
### Clean all installed dependencies
If after upgrading some dependencies, you encountered any issue when boosting the project, you can simply run this command to delete all dependencies and the lock file:
```
yarn clean
```
Then you can run `yarn` to reinstall.

### Compiles and minifies for production
```
yarn build
```
```
yarn build:test
```
```
yarn build:pre
```
```
yarn build:prod
```

### Run your tests
```
yarn test
```

### Lints and fixes files
```
yarn lint
```

### Run your unit tests
```
yarn test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Change Log
#### 2.2.1 - *22 Nov 2021*
+ Add Eslint rule for using single quotes

#### 2.2.0 - *22 Nov 2021*
+ Add Composition API plugin

#### 2.1.1 - *19 Nov 2021*
+ Update comments in events.js

#### 2.1.0 - *19 Nov 2021*
+ Release version 2.1.0
+ Add enhancement hook functions for Vue.js event system
+ Update custom event names as per ADS Developers' Guide

#### 2.0.0 - *18 Oct 2021*
+ Release version 2.0.0

#### 1.2.0 - *13 Oct 2021*
+ Update .eslint.js and .gitignore, add jsconfig.js, update dependency versions

#### 1.1.6 - *26 Feb 2021*
+ Add dynamic <title> for SPA (for accessibility testing)
+ Re-format change log

#### #1.1.5 - *22 Feb 2021*
+ Update google fonts link in `index.html` to fix HTML parsing error (for accessibility testing)

#### #1.1.4 - *05 Feb 2021*
+ Add the detailed comment for `Vue.prototype.$eventHub`

#### #1.1.4 - *26 Nov 2021*
+ Update the usage of the upgraded `MyEssentials` component

#### #1.1.1 - *11 Sep 2020*
+ Small enhancements to the api module and vuex

#### #1.1.0 - *28 Aug 2020*
+ Add .env.* files and scripts for different environments
+ Add a module (in `/src/api`) for central API requests management
+ Modularize API requests per business logic (in `/src/api/modules`)
+ Re-format change log

#### #1.0.32 - *13 Jul 2020*
+ Match `doe-ui-core` version 1.0.32 updates

#### #1.0.11 - *23 Apr 2020*
+ Match `doe-ui-core` version 1.0.11 updates

#### #1.0.9 - *20 Mar 2020*
+ Match `doe-ui-core` version 1.0.9 updates
+ Shift from `unpkg` to `jsdelivr` for app-icons

#### #1.0.6 - *11 Feb 2020*
+ Match `doe-ui-core` version 1.0.6 updates

#### #1.0.5 - *11 Feb 2020*
+ Match `doe-ui-core` version 1.0.5 updates

#### #1.0.4 - *28 Jan 2020*
+ Match `doe-ui-core` version 1.0.4 updates

#### #1.0.2 - *11 Dec 2019*
+ Match `doe-ui-core` version 1.0.2 updates

#### #1.0.1 - *11 Dec 2019*
+ Match `doe-ui-core` version 1.0.1 updates

#### #1.0.2 - *02 Dec 2019*
+ Release version 1.0.0
